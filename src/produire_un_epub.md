# Concepts de base

Le but est de permettre de générer des ePubs 3.0 et/ou 3.2 valides pour l’accessibilité, qui soit conforme aux recommandations du Syndicat National de l’Édition^[Dans la [brochure](https://www.sne.fr/app/uploads/2020/12/ePubAccessibleCharteSNE_v1.0-version-nettoy%C3%A9e_couvOK.pdf) de la Commission Numérique - Groupe Normes et Standards de décembre 2020.] :

- un [EPUBCheck](https://github.com/w3c/epubcheck) sans erreur ni warning
- un niveau WCAG^[Web Content Accessibility Guidelines du World Wide Web Consortium (W3C).] au minimum AA (voire AAA dans la mesure du possible). Ce score s’obtient par le biais du contrôle d’[ACE](https://daisy.github.io/ace/)^[Accessibility Checker for EPUB du Consortium DAISY].

L’architecture du système se base sur l’usage de dépôts Git et d’Intégration Continue sous GitLab. Un dépôt est créé pour chaque projet de livre à modifier ou à créer, à partir d’un gabarit de départ. Un autre dépôt contient les exécutables lancés lors de chaque Intégration Continue afin de générer les objets finaux, à savoir des fichiers ePub essentiellement, mais aussi des rapports d’analyse de leur qualité, éventuellement un fichier LaTeX et un pdf, ainsi que tout ou partie des fichiers intermédiaires de création, à la demande. L’outil permet à des modifications manuelles d’intervenir au cours des processus automatisés afin de générer un ePub au plus près des attentes.

# Le système technique de génération 

Le choix a été retenu de se baser sur une arborescence réfléchie pour fonctionner avec un ensemble de fichiers ``makefile``, et n’intégrant que des commandes les plus simples et directes possibles dans un environnement de type BASH de façon à faciliter la maintenance dans le temps et les soucis de compatibilité. Cela devrait permettre à la fois certaines automatisations tout en laissant la liberté de reprendre manuellement les points nécessaires à la spécificité d’un projet ou l’autre.

## Environnement de travail et logiciels nécessaires

Comme indiqué précédemment, le système d’Intégration Continue contient un environnement de travail de type shell Bash. Une connaissance de cet outil permettra d’en modifier le comportement selon les besoins.. Il est possible que les options retenues soient compatibles avec d’autres interpréteurs de commandes Unix, mais cela devra être vérifié en amont avant de déployer la solution.

Liste des logiciels nécessaires au fonctionnement du makefile :

- pandoc
- zip / unzip
- make
- git
- epubcheck
- ace (via nodejs, voir [la page d’installation du Consortium Daisy](https://daisy.github.io/ace/getting-started/installation/))
- un environnement LaTeX qui comprend xetex/xelatex(texlive-base-bin sous Debian par example) pour générer des fichiers .tex et des .pdf

# Utilisation du système

## Le dépôt avec les utilitaires

Un dépôt avec toutes les commandes a été créé sur [https://framagit.org/YannKervran/pipeline-framabook](https://framagit.org/YannKervran/pipeline-framabook).

Il fournit les outils de base qui seront utilisés au sein de l’Intégration Continue.

## Organisation des répertoires

Les répertoires déployés lors des différentes opérations sont les suivants :

- ``build`` : il contiendra le résultat des commandes passées pour obtenir les fichiers finaux
- ``fromepub`` : il contiendra éventuellement le contenu de l’ePub qu’on souhaite modifier (si on part d’un tel document), avant application des modifications de métadonnées et éventuellement de couverture
- ``deflate`` : il contiendra le résultat d’un ePub tel que généré automatiquement par l’utilisation d’une commande pandoc sur les fichiers source quand on part d’un fichier markdown, et les éléments extraits de l’ePub modifiés des éléments indiqués dans les nouvelles métadonnées et éventuellement de la couverture quand on part d'un ePub
- ``scripts`` : il contient d’éventuels scripts appelés depuis le makefile ou des paramètres pour certaines instructions
- ``tmp`` : il contiendra les fichiers temporaires et intermédiaires

Le dépôt contenant le projet sera basé sur [le gabarit](https://framagit.org/framabook/systeme-de-publication-framabook-gabarit-de-depot) :

- à la racine : un fichier ``README.md`` qui contiendra tous les éléments d’information sur le titre, qui seront ensuite renseignées dans le fichier ``src/informations.yaml``, le fichier ``.gitlab-ci.yml`` qui sert à appeler et configurer l’Intégration Continue.
- ``src`` : il contiendra les fichiers sources, textes en markdown compatible Pandoc^[Tels que décrits sur la page [Pandoc Markdown](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html).] ou le fichier ePub à modifier, le fichier ``informations.yaml``, la possible couverture ``cover.jpg`` et l’éventuel fichier d’expressions régulières ``regexp.sed``
- ``work`` : il contiendra tous les éléments que nous modifierons manuellement en tant que surcouche à ce qui existe dans ``deflate`` pour générer les fichiers finaux. L’arborescence de structure de l’ePub doit être copiée dans ``work/replaced_files`` pour que les fichiers qui y seront placés puissent remplacer ceux de mêmes noms générés automatiquement dans ``deflate``, ou pour s’y ajouter si ceux-ci n’ont pas été générés automatiquement. On trouvera aussi un fichier ``delete_list.txt`` qui contiendra la liste des fichiers à effacer de l’arborescence.


## Les commandes du makefile

## all

Enchaîne les recettes ``firstpass``, ``regexp``, ``preparebuild`` et ``epub`` pour générer un ePub depuis du markdown avec toutes les instructions en place. C’est la commande par défaut qui est lancé si on n’indique rien à l’instruction ``make``.

## allfromepub

Enchaîne les recettes ``dezip``, ``regexp``, ``preparebuild`` et ``epub`` pour modifier un ePub existant selon les instructions en place.

## firstpass

Génère le contenu du répertoire ``deflate`` à partir des fichiers markdown du répertoire ``src``. Ceux-ci doivent être numérotés dans l’ordre d’inclusion si celui-ci a une importance.

## dezip

Extrait le contenu d’un fichier ePub (veiller à ce qu’il soit unique) situé dans le répertoire ``src`` vers le répertoire ``fromepub`` afin d’en permettre la modification/la reprise. Puis il en modifie les métadonnées (fichier ``*.opf`` interne à l’ePub), y insère l’éventuelle nouvelle couverture et place le résultat dans le répertoire ``deflate``. Dans le cas de changement de couverture, il insère des expressions régulières dans le fichier ``src/regexp.sed`` pour qu’elles modifient, lors de l'appel de la recette ``regexp``, les appels en conséquence dans les différents fichiers.

## regexp

Applique les expressions régulières indiquées dans ``scripts/regexp.sed`` à l’ensemble des fichiers de ``deflate``.

## preparebuild

Construit dans ``tmp/epub`` l’arborescence des fichiers qui sera placée dans l’ePub, avec le contenu de ``deflate`` (dont il dépend), sur lequel celui de ``work/replaced_files`` vient s’ajouter. Si ce dernier répertoire contient des fichiers de même nom que ceux de ``deflate``, ils seront remplacés.

Il efface également du répertoire ``tmp/epub`` les fichiers listés un par ligne dans le fichier ``work/delete_list.txt``, avec leur chemin interne relatif.

## epub

Construit dans le répertoire ``build`` un fichier ePub nommé selon l’indication ``title`` du fichier ``src/informations.yaml``. Il se base sur le contenu du répertoire ``tmp/epub`` mis en place par sa dépendance, ``preparebuild``.

## validate

Teste les fichiers ePub du répertoire ``build`` pour valider leur contenu :

- ``epubcheck`` avec une sortie dans un fichier ``build/epucheck_report.json``
- ``ace`` avec une sortie json et hml dans le répertoire ``build/ace_reports``

## latex

Crée le sous-répertoire ``tex`` s’il n’existe pas déjà, et y crée un fichier ``.tex`` nommé selon l’indication ``title`` du fichier ``src/informations.yaml`` suivant les instructions du gabarit fourni dans le dépôt du makefile. Attention cette recette ne peut être appelée que si on fournit un fichier markdown en entrée, pas un ePub.

## pdf

Compile le fichier ``monlivrefinal.tex`` du sous-répertoire ``tex`` en un fichier ``build/monlivrefinal.pdf``. Il se trouvera également dans le sous-répertoire ``tex`` les fichiers intermédiaires rendus nécessaires par cette génération. Dans le cas où l’ouvrage contient une bibliographie, une table des matières ou une liste des illustrations, il est nécessaire de lancer la commande deux fois pour que tout soit correctement généré.

## clean

Efface le contenu des répertoires ``deflate``, ``fromepub``, ``build`` et ``tmp`` pour opérer sur une base propre.

Attention, cette commande n’efface rien dans les répertoires suivants, dont tout ou partie a été généré automatiquement, mais qui peuvent avoir fait l’objet de reprises manuelles :

- ``/tex``
- ``/work/replaced_files/``
- ``/src/regexp.sed`` a pu être peuplé de modifications à intégrer lors de la décompression d’un ePub à modifier.

# Marche à suivre pour un nouveau projet

## Création du dépôt de projet

Pour les projets internes à Framabook, il faut créer celui-ci dans le sous-Groupe destiné aux ouvrages : [https://framagit.org/framabook/ouvrages](https://framagit.org/framabook/ouvrages)

Pour chaque nouveau livre ou reprise de livre, il faut créer un dépôt qui soit basé sur le gabarit fourni : [https://framagit.org/framabook/systeme-de-publication-framabook-gabarit-de-depot](https://framagit.org/framabook/systeme-de-publication-framabook-gabarit-de-depot)

Pour ce faire, il suffit de cliquer sur GitLab sur « Nouveau projet/New project » > « Import project » > « Repo by URL » et d'y indiquer l'adresse « https://framagit.org/framabook/systeme-de-publication-framabook-gabarit-de-depot.git », tout en prenant garde à changer le nom de votre dépôt de destination dans « Project name ».

### Le contenu originel d'un dépôt de projet

Le fichier ``README.md`` y présentera toutes les informations utiles sur le projet, il ne sert pas à proprement parler à générer les fichiers finaux. Il ne sert que de référence.

Inclure les fichiers de licence appliqués au contenu permettra aussi de s’y référer le cas échéant. Pour rappel, les principales licences généralement rencontrées dans nos projets sont :

Avec copyleft, privilégiées pour la défense des communs culturels :

- la Licence Art Libre - [Texte complet](https://artlibre.org/)
- la licence Attribution - Partage dans les mêmes conditions / CC BY SA - [Résumé](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) - [Texte complet](https://creativecommons.org/licenses/by-sa/4.0/legalcode.fr)
- la licence GNU de documentation libre - GNU FDL - [Texte complet](https://www.gnu.org/licenses/fdl-1.3.fr.html)

Sans copyleft, mais compatibles avec les communs culturels :

- la licence Attribution / CC BY - [Résumé](https://creativecommons.org/licenses/by/4.0/deed.fr) - [Texte complet](https://creativecommons.org/licenses/by/4.0/legalcode.fr)
- la licence Transfert dans le Domaine Public / CC0 - [Résumé](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) - [Texte complet](https://creativecommons.org/publicdomain/zero/1.0/legalcode.fr)

On trouve également le fichier ``.gitlab-ci.yml`` qui contient les instructions pour l’Intégration Continue, qui n’est pas à modifier.

### Production d’ePub à partir de sources en markdown

Il faut déposer dans le répertoire ``src`` tout ce qui servira de point de départ au projet, selon les cas :

- les fichiers markdown, numérotés dans leur ordre d’inclusion (obligatoire).
- un fichier YAML ``informations.yaml`` avec les indications de métadonnées nécessaires (obligatoire).
- un fichier ``cover.jpg`` qui servira de couverture à l’ePub généré depuis les fichiers markdown.
- un fichier ``regexp.sed`` qui contient toutes les expressions régulières à appliquer dans les fichiers internes

#### Génération de pdf

Cette possibilité n’est offerte que si on travaille à partir de fichiers markdown.

Il faut adjoindre au fichier YAML ``informations.yaml`` une partie avec les informations nécessaires au gabarit fourni par défaut par l’Intégration Continue (``[templates/book.latex](https://framagit.org/framabook/systeme-publication-framabook/-/tree/main/templates)``)
    
    ---
    #### Champs pour LaTeX /pdf ####
    documentclass: report # report a pour effet de passer la hiérarchies des titres de niveau 1 à Chapter ou Part. Ne rien toucher.
    papersize: A5 # Si rien d’indiqué, A4 par défaut
    fontsize: 12pt # taille de la police
    #linestretch: 1 # interligne (par défaut : 1.1)
    small-title: "Produire un ePub Framabook" # version courte du titre pour les en-têtes (obligatoire)
    info-sup: "Éditions Framabook" # Renseigner les infos supplémentaires qui figureront sur la page de titre, vers le bas de la page
    titlepage: true # true or false : si on veut ou non une page de titre
    toc: true # si on veut ou non une table des matières
    toc-title: "Table des matières" # renommer la table des matières
    toc-own-page: true # si on veut que la table des matières ai sa propre page
    toc-depth: 4 # niveaux de titres dans la table des matières (n``oubliez pas qu``en réduisant les niveaux, cela devient un sommaire :))
    #lof: true # Liste des figures 
    #lot: true # Liste des tables
    numbersections: true # Si on numérote les sections
    secnumdepth: 2 # Profondeur de numérotation des sections
    links-as-notes: true
    mainfont: "Alegreya" # Sélection de la police principale (vérifier que la police est installée sur le système!)
    #mainfontoptions: "Scale=1.0" # échelle de la police (pas vraiment nécessaire)
    sansfont: "Noto Sans" # sélection de la police sans serif (vérifier que la police est installée sur le système!)
    monofont: "Noto Sans" # sélection de la police mono (vérifier que la police est installée sur le système!)
    # mathfont: "xxxxxxxxxxx" # sélection de la police pour maths  (vérifier que la police est installée sur le système!)
    
    ---


### Modification d’un ePub déjà existant

Il faut déposer dans le répertoire ``src`` :

- le fichier ePub original dont il s’agit de remanier le contenu. Il ne doit y en avoir qu’un seul dans le répertoire (obligatoire)
- les métadonnées dans un fichier YAML ``informations.yaml`` avec les indications de métadonnées nécessaires (obligatoire)
- un éventuel nouveau fichier de couverture, au format jpg, et nommé ``cover.jpg'
- un éventuel fichier d’expressions régulières ``regexp.sed`` à appliquer.

### Le formatage du YAML

Les métadonnées de l’édition doivent être fournies dans un fichier rédigé en [YAML](https://fr.wikipedia.org/wiki/YAML) contenant les informations nécessaires, telles que présenté ci-dessous. Il doit impérativement être nommé ``informations.yaml`` et comporter ces champs, correctement renseignés comme dans l’exemple ci-dessous :

    author: # Renseigner auteurs et autrices (obligatoire)
    - Yann Kervran
    - Jean Bernard Marcon
    - Christophe Masutti
    rights: Creative Commons BY SA
    description: Manuel de création d’ePubs
    title: Produire un ePub Framabook # renseigner le titre (obligatoire)
    lang: fr-FR # renseigner la langue du doc (obligatoire)
    subject: ePub, Framabook, Framasoft
    date: 2020-03-08 # Renseigner la date (obligatoire)
    abstract: Ce manuel fournit les instructions de base pour déployer un système de production d’ePubs conformes aux recommandations d’accessibilité en ne se basant que sur des outils en ligne de commande dans un terminal shell Bash.
    version: "V. 0.96" # Version du document
    collection: Manuel # Collection de l’ouvrage : Essai, Manuel, Roman ou BD (obligatoire)

### Le formatage du markdown

Les fichiers en markdown seront concaténés lors de la création de l’ePub, en se basant sur leurs noms pour l’ordre. Il faut donc veiller à les organiser de la façon souhaitée, en les numérotant, par exemple. Attention à bien vérifier qu’il s’agit de markdown compatible Pandoc[^1].

[^1]: Tel que décrit sur la page [Pandoc Markdown](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html).

### Le formatage des expressions régulières

Les expressions régulières seront appliquées sur le contenu des fichiers situés dans ``deflate`` par l’utilitaire [``sed``](https://www.gnu.org/software/sed/) depuis le fichier ``regexp.sed``, ligne par ligne. Des commentaires peuvent être insérés en disposant un caractère « # » en début de ligne.

Ensuite, chaque expression régulière devra impérativement être de la forme «s -tabulation-source-tabulation-destination-tabulation-retour chariot » :

    s	source	cible	

Notez que chaque élément est séparé du précédent par une tabulation et qu’il y en a également une avant le retour à la ligne, qui est indispensable.

    # Regexp qui s’appliquent dans le répertoire deflate (ne pas oublier la tabulation finale)
    # Exemple (changement de chemin d’accès pour des fichiers d’images):
    #s	../images/encarts	images/encarts	

### La préparation de l’image

Une image nommée ``cover.jpg`` servira de couverture. Elle doit être au format jpg, compressée à 90%, faire 559 pixels de large par 794 de large et être en RVB pour les ouvrages Framabook.

## Utilisation de l’Intégration Continue

Le déclenchement de l’Intégration Continue se fait par l’intermédiaire de mots-clefs insérés dans le message de commit :

- \<buildepub> : permet de générer un fichier ePub à partir de sources en markdown
- \<buildfromepub> : permet de générer un fichier ePub modifié à partir d’un ePub source

TBD:

- mot-clef pour générer la génération des rapports d’analyse DAISY et W3C (séparément ou les deux)
- mot-clef pour générer pdf et/ou LaTeX
- mot-clef pour générer le contenu de ``deflate`` pour permettre son analyse/reprise via les expressions régulières et/ou le répertoire ``work``


## Utilisation du système en local

Il est possible de ne pas utiliser le système d’Intégration Continue et de se servir uniquement des recettes du makefile, pour éventuellement l’adapter à des besoins propres. Il convient alors de récupérer le contenu du [dépôt du système ](https://framagit.org/framabook/systeme-publication-framabook) et d’en supprimer les fichiers :

- ``.gitignore``
- ``README.md``

Le reste doit se placer à la racine du dépôt de projet, pour avoir une arborescence finale du type :

- ``makefile``
- ``README.md``
- ``LICENSE``
- ``/templates/book.latex``
- ``/src/montexte.md``
- ``/src/informations.yaml``
- ``/src/cover.jpg``

On lancera ensuite les commandes makefile souhaitées pour ses besoins. Le schéma intégré peut aider à se repérer parmi les recettes.

### Mise en place du contenu de l’ePub depuis du markdown

Pour générer l’ePub, il suffit de se placer dans le répertoire maître et de taper la commande :

    make firstpass

On obtient alors dans le sous-répertoire ``deflate`` le contenu du futur ePub.


### Mise en place du contenu de l’ePub depuis un fichier ePub à modifier

Pour récupérer le contenu de l’ePub, il suffit de se placer dans le répertoire maître et de taper la commande :

    make dezip

On obtient alors dans le sous-répertoire ``deflate`` le contenu du futur ePub.

### Génération de l’ePub

Pour générer l’ePub, il suffit de se placer dans le répertoire maître et de taper la commande :

    make epub

On obtient alors dans le sous-répertoire ``build`` le fichier nommé selon la métadonnée ``title`` du fichier YAML. Il est possible de se contenter de cela et de voir si le résultat est directement valide. Si ce n’est pas le cas, il va falloir retoucher par endroit le contenu par des modifications manuelles.

### Modifications intermédiaires

#### Ajout ou modification d’un fichier

Il est possible d’ajouter ou de modifier un fichier présent dans l’arborescence de l’ePub en disposant son remplaçant au même endroit relatif dans ``work/replaced_files``. Ainsi, pour remplacer le fichier qui se situe à l’emplacement ``EPUB/text/title_page.xhtml`` dans l’ePub, nous allons placer son remplaçant dans ``work/replaced_files/EPUB/text/title_page.xhtml``. On peut se contenter de copier/coller l’arborescence contenue dans ``deflate`` dans ``work/replaced_files`` et d’en effacer tous les fichiers que l’on ne souhaite pas reprendre manuellement afin d’être certain d’avoir la bonne structure hiérarchique.

#### Effacement d’un fichier

Il suffit de créer un fichier ``delete_list.txt`` dans le sous-répertoire ``work``, et qui contiendra les adresses relatives des fichiers à supprimer de l’ePub. Ainsi, pour effacer le fichier qui se situe à l’emplacement ``EPUB/test.txt`` dans l’ePub, nous allons y ajouter une ligne indiquant ``EPUB/test.txt``.

### Contrôle et validation du fichier ePub généré

Toujours depuis le répertoire principal, il faut taper la commande :

    make validate

On obtient alors :

- un fichier dans le sous-répertoire ``build`` nommé ``epubcheck_report.json`` généré par [EPUBCheck](https://github.com/w3c/epubcheck).
- un sous-répertoire ``build/ace_reports`` qui contient les fichiers de rapport de l’[ACE](https://daisy.github.io/ace/)^[Accessibility Checker for EPUB du Consortium DAISY].

### La recette globale ``make all``

À noter que la commande :

    make all

va effectuer l’intégralité des recettes générant l’ePub depuis du markdown, en une seule commande, en appliquant d’éventuelles expressions régulières et modifications de fichiers intermédiaires. Cela permet de voir rapidement le résultat obtenu de façon automatique.

Pour faire la même chose depuis un fichier ePub, c’est la commande :

    make allfromepub

### Créer un pdf depuis le markdown

On tape la commande :

    make pdf

afin d’obtenir dans ``build`` un fichier pdf nommé selon l’indication ``title`` du fichier ``src/informations.yaml``. Si on souhaite intervenir sur le fichier ``.tex`` intermédiaire, il faut taper une première commande :

    make tex

pour l’obtenir dans le sous-répertoire ``tex``. On peut alors intervenir dessus manuellement avant de générer le pdf avec la commande ``make pdf``. Dans tous les cas, le fichier généré automatiquement risque de comporter des imperfections qui ne sauraient être prises en charge par la génération automatique.

Attention : si on éxécute de nouveau ``make tex`` alors qu’on avait modifié le fichier .tex manuellement, lesmodifications seront écrasées par la génération automatique.

# Schéma détaillant les recettes

![](schema_makefile.png)
