Ce dépôt est basé sur l’utilisation du [Système de publication Framabook](https://framagit.org/framabook/systeme-publication-framabook) et en constitue le manuel.

Titre :

- Produire un ePub

Auteurs : 

- Yann Kervran
- Jean-Bernard Marcon
- Christophe Masutti

Éditeur :

- Yann Kervran

Publicateur :

- Yann Kervran

Date de sortie :

- Juin 2021

ISBN :

Licence de l’ouvrage :

- [Creative Commons BY SA](https://creativecommons.org/licenses/by-sa/2.0/fr/)

Sujet:

- manuel, édition, publication (exemples)

Résumé (une ou deux phrases):

- Ce manuel fournit les instructions de base pour déployer un système de production d’epubs conformes aux recommandations d’accessibilité en ne se basant que sur des outils en ligne de commande dans un terminal shell Bash.

Version:

- 0.5
